import java.util.Scanner;

public class FahrenheitCelsius {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter degrees Fahrenheit: ");
        double fahrenheit = in.nextInt();

        final double FAHREINHEIT_PER_CELSIUS = 1.8;
        final int FREEZ_POINT_OF_WATER = 32;

        double celsius;

        celsius = (fahrenheit - FREEZ_POINT_OF_WATER) / FAHREINHEIT_PER_CELSIUS;
        System.out.println(fahrenheit  + " degrees Fahrenheit = " + celsius + " degrees Celsius");
    }
}

